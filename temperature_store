ruleset temperature_store {
  meta {
    shares __testing, temperatures, threshold_violations, inrange_temperatures
    provides temperatures, threshold_violations, inrange_temperatures
    use module io.picolabs.subscription alias Subscriptions
    
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }, 
      { "name": "temperatures", "args": [ ] }
      ] , "events":
      [ //{ "domain": "d1", "type": "t1" }
        { "domain": "sensor", "type": "reading_reset"},
        { "domain": "wovyn", "type": "report", "attrs": ["call_back_id", "request_id"]}
      ]
    }
    
    temperatures = function() {
      //A function called temperatures that returns the contents of the temperature entity variable.
      ent:temperatures.defaultsTo([]).klog("Call to temperatures() returned: ")
    }
    
    threshold_violations = function() {
      //that returns the contents of the threshold violation entity variable.
      ent:violations.defaultsTo([]).klog("Call to threshold_violations() returned: ")
    }
    
    inrange_temperatures = function() {
      //returns all the temperatures in the temperature entity variable that aren't in the threshold violation entity variable.
      //A.difference(B);
      ent:temperatures.defaultsTo([]).difference(ent:violations.defaultsTo([])).defaultsTo([]).klog("Call to inrange_temperatures() returned: ")
    }
  }
  
  rule temperature_report_requested {
    select when wovyn report
    pre {
      call_back = event:attrs{"call_back_id"}
      my_id = Subscriptions:established("Tx_role","controller")[0]{"Rx"}
      temps = temperatures()
      request_id = event:attrs{"request_id"}
    }
    event:send({
        "eci": call_back, 
        "domain":"sensor", 
        "type":"submit_report", 
        "attrs":{
          "request_id": request_id,
          "Sensor_Id": my_id,
          "temperatures": temps
        }
      })
  }
  
  rule collect_temperatures {
    select when wovyn new_temperature_reading
    pre {
      tempF = event:attrs{"temperature"}
      timestamp = event:attrs{"timestamp"}
      entry = {"tempF":tempF, "timestamp":timestamp}
    }
    always {
     ent:temperatures := ent:temperatures.defaultsTo([]).append(entry).klog("New Temp Added");
    }
  }
  
  rule collect_threshold_violations {
    select when wovyn threshold_violation
    pre {
      tempF = event:attrs{"temperature"}
      timestamp = event:attrs{"timestamp"}
      entry = {"tempF":tempF, "timestamp":timestamp}
    }
    always {
      ent:violations := ent:violations.defaultsTo([]).append(entry).klog("Violation Added");
    }
  }
  
  rule clear_temperatures {
    select when sensor reading_reset
    always {
      ent:violations := ent:violations.defaultsTo([]);
      ent:temperatures := ent:temperatures.defaultsTo([]);
      clear ent:violations;
      clear ent:temperatures;
    }
  }
}
